package com.xy.mmall.common;


import com.google.common.cache.CacheBuilder;
import com.google.common.cache.CacheLoader;
import com.google.common.cache.LoadingCache;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.concurrent.TimeUnit;

/**
 * @Auther: xy
 * @Date: 2018/10/23 16:40
 * @Description:
 */
public class TokenCache {

    public static final String TOKEN_PREFIX = "token_";
    private static Logger logger = LoggerFactory.getLogger(TokenCache.class);

    //LRU算法
    private static LoadingCache<String,String> loadingCache = CacheBuilder.newBuilder().initialCapacity(1000).maximumSize(10000).expireAfterWrite(12, TimeUnit.HOURS)
            .build(new CacheLoader<String, String>() {
                //默认加载数据实现,当调研get取值时,如果key没有找打想要的值,就调研这个方法加载
                @Override
                public String load(String key) throws Exception {
                    return "null";
                }
            });
    public static void setKey(String key,String value){
        loadingCache.put(key,value);
    }
    public static  String getKye(String key) {
        String value = null;
        try {
            value = loadingCache.get(key);
            if (value.equals(null)){
                return null;
            }
            return value;
        }catch (Exception e){
            logger.error("localCache get error" ,e);
        }
        return null;
    }
}
