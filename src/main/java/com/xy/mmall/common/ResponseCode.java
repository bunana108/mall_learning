package com.xy.mmall.common;

/**
 * @Auther: xy
 * @Date: 2018/10/23 09:26
 * @Description:
 */
public enum  ResponseCode {

    SUCCESS(0,"SUCCESSS"),
    ERROR(1,"ERROR"),
    NEED_LOGIN(10,"NEDD_LOGIN"),
    ILLEGAL_ARGUMENT(2,"ILLEGAL_ARGUMENT");

    private final int code;
    private final String desc;

    ResponseCode(int code, String desc) {
        this.code = code;
        this.desc = desc;
    }
   public int getCode() {
        return code;
    }

    public String getDesc() {
        return desc;
    }

}
