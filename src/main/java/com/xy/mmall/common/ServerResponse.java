package com.xy.mmall.common;

import org.codehaus.jackson.annotate.JsonIgnore;
import org.codehaus.jackson.map.annotate.JsonSerialize;

import java.io.Serializable;

/**
 * @Auther: xy
 * @Date: 2018/10/23 09:19
 * @Description:
 */
@JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL)
//保证序列化json的时候如果是null对象key也会消失
public class ServerResponse <T> implements Serializable {

    private int state;
    private String msg;
    private T date;

    private ServerResponse(int state){
        this.state = state;
    }
    private ServerResponse(int state,T date){
        this.state =state;
        this.date = date;
    }
    private ServerResponse (int state ,String msg,T date){
        this.state = state;
        this.msg = msg;
        this.date =date;
    }
    private ServerResponse(int state ,String msg){
        this.state =state ;
        this.msg = msg;
    }
    @JsonIgnore
    //使之不在序列号结果当中
    public  boolean isSuccess(){
        return  this.state == ResponseCode.SUCCESS.getCode();
    }
    public int getState(){
        return  state;
    }
    public  T getDate(){
        return  date;
    }
    public  String getMsg(){
        return  msg;
    }
    public static  <T>ServerResponse<T> createBySuccess(){
        return new ServerResponse<T>(ResponseCode.SUCCESS.getCode());
    }
    public static <T>ServerResponse<T> createBySuccessMessage(String msg){
        return  new ServerResponse<T>(ResponseCode.SUCCESS.getCode(),msg);
    }
    public static <T>ServerResponse<T> createBySuccess(T date){
        return new ServerResponse<T>(ResponseCode.SUCCESS.getCode(),date);
    }
    public static <T>ServerResponse<T> createBySuccess(String msg,T date){
        return new ServerResponse<T>(ResponseCode.SUCCESS.getCode(),msg,date);
    }

    public static <T>ServerResponse<T> createByErrorMessage(){
        return new ServerResponse<T>(ResponseCode.ERROR.getCode(),ResponseCode.ERROR.getDesc());
    }
    public static <T>ServerResponse<T> createByErrorMessage(String errorMessage){
        return new ServerResponse<T>(ResponseCode.ERROR.getCode(),errorMessage);
    }
    public static <T>ServerResponse<T> createByErrorCodeMessage(int errorCode,String errorMessgae){
        return new ServerResponse<T>(errorCode,errorMessgae);
    }


}

