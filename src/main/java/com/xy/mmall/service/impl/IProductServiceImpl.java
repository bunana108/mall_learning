package com.xy.mmall.service.impl;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.google.common.collect.Lists;
import com.xy.mmall.common.ResponseCode;
import com.xy.mmall.common.ServerResponse;
import com.xy.mmall.dao.CategoryMapper;
import com.xy.mmall.dao.ProductMapper;
import com.xy.mmall.pojo.Category;
import com.xy.mmall.pojo.Product;
import com.xy.mmall.service.IProductService;
import com.xy.mmall.util.DateTimeUtil;
import com.xy.mmall.util.PropertiesUtil;
import com.xy.mmall.vo.ProductDetailVo;
import com.xy.mmall.vo.ProductListVo;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @Auther: xy
 * @Date: 2018/11/15 15:17
 * @Description:
 */
@Service("iProductService")
public class IProductServiceImpl implements IProductService {

    @Autowired
    private ProductMapper productMapper;

    @Autowired
    private CategoryMapper categoryMapper;
    /**
     * 更新或者新增产品
     * @param product
     * @return
     */
    @Override
    public ServerResponse saveOrUpdateProduct(Product product){
        //判断参数是否为空
        if(product == null){
            //判断图片路径是否为空
           if (StringUtils.isNotBlank(product.getSubImages())){
               String [] subImageArray = product.getSubImages().split(",");
               //将第一张图pain赋值给MainImage
               product.setMainImage(subImageArray[0]);
           }
           if (product.getId() != null){
               //修改
               int rowCount = productMapper.updateByPrimaryKey(product);
               if (rowCount > 0){
                   return ServerResponse.createBySuccessMessage("更新成功");
               }
               return ServerResponse.createByErrorMessage("更新失败");
           }
           else {
               //添加
               int rowCount = productMapper.insert(product);
               if (rowCount > 0){
                   return ServerResponse.createBySuccessMessage("新增成功成功");
               }
               return ServerResponse.createByErrorMessage("新增失败");
           }
        }
        return ServerResponse.createByErrorMessage("新增或者更新参数错误");
    }

    /**
     * 修改产品销售状态
     * @param productId
     * @param status
     * @return
     */
    @Override
    public ServerResponse<String> setSaleStatus(Integer productId,Integer status ){
        if (productId ==null || status == null){
            return  ServerResponse.createByErrorCodeMessage(ResponseCode.ILLEGAL_ARGUMENT.getCode(),ResponseCode.ILLEGAL_ARGUMENT.getDesc());
        }
        Product product = new Product();
        product.setId(productId);
        product.setStatus(status);
        int rowCount = productMapper.updateByPrimaryKey(product);
        if (rowCount > 0){

            return  ServerResponse.createBySuccess("修改产品销售状态成功");
        }
        return ServerResponse.createByErrorMessage("修改产品销售状态失败");
    }


    @Override
    public ServerResponse<ProductDetailVo> manageProductDetail(Integer productId){
        if (productId == null){
            return ServerResponse.createByErrorCodeMessage(ResponseCode.ILLEGAL_ARGUMENT.getCode(),ResponseCode.ILLEGAL_ARGUMENT.getDesc());
        }
        Product product = productMapper.selectByPrimaryKey(productId);
        if (product == null){
            return ServerResponse.createByErrorMessage("产品下架或者已经删除");
        }
        ProductDetailVo productDetailVo = assembleProductDetailVo(product);
        return ServerResponse.createBySuccess(productDetailVo);
    }

    private ProductDetailVo assembleProductDetailVo(Product product){
        ProductDetailVo productDetailVo = new ProductDetailVo();
        productDetailVo.setId(product.getId());
        productDetailVo.setCategoryId(product.getCategoryId());
        productDetailVo.setSubtitle(product.getSubtitle());
        productDetailVo.setMainImage(product.getMainImage());
        productDetailVo.setName(product.getName());
        productDetailVo.setPrice(product.getPrice());
        productDetailVo.setDetail(product.getDetail());
        productDetailVo.setStatus(product.getStatus());
        productDetailVo.setStock(product.getStock());
        productDetailVo.setSubImages(product.getSubImages());

        productDetailVo.setImageHost(PropertiesUtil.getProperty("ftp.server.http.prefix","http://img.happymmall.com/"));

        Category category = categoryMapper.selectByPrimaryKey(product.getCategoryId());
        if (category == null){
            //默认根节点
            productDetailVo.setCategoryId(0);
        }
        else{
            productDetailVo.setCategoryId(category.getParentId());
        }
        productDetailVo.setCreateTime(DateTimeUtil.dateToStr(category.getCreateTime()));
        productDetailVo.setUpdateTime(DateTimeUtil.dateToStr(category.getUpdateTime()));


        return  productDetailVo;

    }
   @Override
    public ServerResponse<PageInfo> getProductList(int pageNum,int pageSize){

        PageHelper.startPage(pageNum, pageSize);
        List<Product> products = productMapper.selectList();
        List<ProductListVo> productListVos = Lists.newArrayList();
        for (Product product : products){
            ProductListVo productListVo = assembleProductListVo(product);
            productListVos.add(productListVo);
        }
        PageInfo pageResult = new PageInfo(products);
        pageResult.setList(productListVos);
        return ServerResponse.createBySuccess(pageResult);

    }


    private ProductListVo assembleProductListVo(Product product){
        ProductListVo productListVo = new ProductListVo();
        productListVo.setName(product.getName());
        productListVo.setId(product.getId());
        productListVo.setCategoryId(product.getCategoryId());
        productListVo.setMainImage(product.getMainImage());
        productListVo.setSubtitle(product.getSubtitle());
        productListVo.setPrice(product.getPrice());
        productListVo.setStatus(product.getStatus());
        productListVo.setImageHost(PropertiesUtil.getProperty("ftp.server.http.prefix","http://img.happymmall.com/"));

        return productListVo;

    }

    public  ServerResponse<PageInfo> searchProducts(String productName,Integer productId,Integer pageNum,Integer pageSize){

        PageHelper.startPage(pageNum,pageSize);
        if (StringUtils.isNotBlank(productName)){
            productName = new StringBuilder().append("%").append(productName).append("%").toString();
        }
        List<ProductListVo> productListVos = Lists.newArrayList();
        List<Product> productList = productMapper.selectByNameAndProductId(productName, productId);
        for (Product product : productList){
            ProductListVo productListVo = assembleProductListVo(product);
            productListVos.add(productListVo);
        }
        PageInfo pageResult = new PageInfo(productList);
        pageResult.setList(productListVos);
        return ServerResponse.createBySuccess(pageResult);
    }


}
