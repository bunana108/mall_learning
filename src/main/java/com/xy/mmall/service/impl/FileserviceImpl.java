package com.xy.mmall.service.impl;

import com.google.common.collect.Lists;
import com.xy.mmall.service.IFileService;
import com.xy.mmall.util.FTPUtil;
import org.codehaus.jackson.map.ser.std.StdJdkSerializers;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.IOException;
import java.util.UUID;

/**
 * @Auther: xy
 * @Date: 2018/12/2 22:22
 * @Description:
 */
@Service("iFileService")
public class FileserviceImpl implements IFileService {

    private Logger logger = LoggerFactory.getLogger(FileserviceImpl.class);

    @Override
    public  String upload (MultipartFile file,String path){
        String fileName = file.getOriginalFilename();
        //扩展名
        String fileExtensionName = fileName.substring(fileName.lastIndexOf(".")+1);
        String uploadFileName = UUID.randomUUID().toString()+"."+fileExtensionName;
        logger.info("开始上传文件,上传的文件名为:{},上传的路径:{},新的文件名:{}",fileName,path,uploadFileName);
        File fileDir = new File(path);
        if (!(fileDir.exists())){
            fileDir.setWritable(true);
            fileDir.mkdirs();
        }
        File targetFile = new File(path,uploadFileName);
        try {
            file.transferTo(targetFile);
            logger.info("上传文件成功");
            //将 targetFile上传到我们的FTP服务器上
            FTPUtil.uploadFile(Lists.newArrayList(targetFile));
            //已经上传到服务器上



           // 上传完之后删除upLoad下面的文件
            targetFile.delete();

        } catch (IOException e) {
           logger.error("上传文件异常",e);
           return  null;
        }

        return targetFile.getName();

    }


}
