package com.xy.mmall.service.impl;

import com.google.common.collect.Lists;
import com.google.common.collect.Sets;
import com.xy.mmall.common.ServerResponse;
import com.xy.mmall.dao.CategoryMapper;
import com.xy.mmall.pojo.Category;
import com.xy.mmall.service.ICategoryService;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import java.util.Collections;
import java.util.List;
import java.util.Set;

/**
 * @Auther: xy
 * @Date: 2018/11/11 22:10
 * @Description:
 */
@Service("ICategoryService")
public class ICategoryServiceImpl  implements ICategoryService {

    private Logger logger = LoggerFactory.getLogger(ICategoryServiceImpl.class);

    @Autowired
    private CategoryMapper categoryMapper;

    /**
     * 添加分类
     * @param categoryName
     * @param parentId
     * @return
     */
    @Override
    public ServerResponse addCategory(String categoryName, Integer parentId) {
        //判断Id是否为空或者 name是否为空
        if(parentId == null || StringUtils.isBlank(categoryName)){
            return  ServerResponse.createByErrorMessage("参数错误");
        }
        else{
            Category category =new Category();
            category.setName(categoryName);
            category.setParentId(parentId);
            //说明这个分类是可用的
            category.setStatus(true);
            int rowCount = categoryMapper.insert(category);

            if (rowCount > 0){
                return  ServerResponse.createBySuccessMessage("添加品类成功");
            }
            return ServerResponse.createBySuccessMessage("添加品类失败");
        }
    }

    /**
     * 修改商品
     * @param categoryId
     * @param categoryName
     * @return
     */
    @Override
    public  ServerResponse updateCategoryName(Integer categoryId,String categoryName){

        if (categoryId == null || StringUtils.isBlank(categoryName)){

            return ServerResponse.createByErrorMessage("更新参数错误");
        }
        else {
          //  Category category = categoryMapper.selectByPrimaryKey(categoryId);
            Category category =new Category();
            category.setParentId(categoryId);
            category.setName(categoryName);
            int rowCount = categoryMapper.updateByPrimaryKeySelective(category);
            if (rowCount > 0){
                return  ServerResponse.createBySuccess("更新成功");
            }
            return ServerResponse.createByErrorMessage("更新失败");
        }

    }

    /**
     * 获取当前节点的category信息
     * @param categoryId
     * @return
     */
    @Override
    public  ServerResponse<List<Category>> getChildrenParallCategory(Integer categoryId){
        List<Category> categoryList = categoryMapper.selectCategoryChildrenByParentId(categoryId);
        //判断集合是否为空
        if (CollectionUtils.isEmpty(categoryList)){
            logger.info("未找到当前分类的子集合");
        }
        return ServerResponse.createBySuccess(categoryList);
    }

    /**
     * 递归查询本节点的id及孩子的节点id
     * @param categoryId
     * @return
     */
    @Override
    public ServerResponse selectCategoryAndChildrenById(Integer categoryId){
        Set<Category> categorySet = Sets.newHashSet();
        findChildCategory(categorySet,categoryId);

        List<Integer> caIntegerIdList = Lists.newArrayList();
        if (caIntegerIdList != null){
            for ( Category categoryItem : categorySet){

                caIntegerIdList.add(categoryItem.getId());
            }
        }
        return  ServerResponse.createBySuccess(caIntegerIdList);

    }

    /**
     * 递归算法,算出子节点
     * @param categorySet
     * @param categoryId
     * @return
     */
    private Set<Category> findChildCategory(Set<Category> categorySet,Integer categoryId){

        Category category = categoryMapper.selectByPrimaryKey(categoryId);
        if (category != null){
            categorySet.add(category);
        }
        //查找子节点,递归算法一定有一个退出的条件
        List<Category> categoryList = categoryMapper.selectCategoryChildrenByParentId(categoryId);
        for (Category categoryItem : categoryList){

            findChildCategory(categorySet,categoryItem.getParentId());
        }
        return categorySet;
    }
}
