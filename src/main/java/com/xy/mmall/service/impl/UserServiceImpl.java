package com.xy.mmall.service.impl;

import com.xy.mmall.common.Conts;
import com.xy.mmall.common.ServerResponse;
import com.xy.mmall.common.TokenCache;
import com.xy.mmall.dao.UserMapper;
import com.xy.mmall.pojo.User;
import com.xy.mmall.service.IUserService;
import com.xy.mmall.util.MD5Util;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.UUID;

/**
 * @Auther: xy
 * @Date: 2018/10/23 09:18
 * @Description:
 * @author xy
 */
@Service("iUserService")
public class UserServiceImpl implements IUserService {

    @Autowired
    private UserMapper userMapper;

    /**
     * 登陆业务逻辑
     * @param username
     * @param password
     * @return
     */
    @Override
    public ServerResponse<User> login(String username, String password) {
        int resultCount = userMapper.checkUsername(username);
        if (resultCount == 0){
            return ServerResponse.createByErrorMessage("用户名不存在");
        }
        //
        String passwordMD5 = MD5Util.MD5EncodeUtf8(password);
        User user = userMapper.selectUser(username, passwordMD5);
        if (user == null){
            return ServerResponse.createByErrorMessage("密码错误");
        }
        user.setPassword(StringUtils.EMPTY);

        return ServerResponse.createBySuccess("登陆成功",user);
    }

    /**
     * 注册业务逻辑
     * @param user
     * @return
     */
    @Override
    public ServerResponse<String> register(User user){
        ServerResponse<String> vaildResponse = this.checkVaild(user.getUsername(), Conts.USERNAME);
        if (! vaildResponse.isSuccess()){
            return vaildResponse;
        }
       vaildResponse = this.checkVaild(user.getEmail(), Conts.EMAIL);
        if (! vaildResponse.isSuccess()){
            return vaildResponse;
        }
        user.setRole(Conts.Rolw.ROLE_CUSTOMER);

        //MD5 密码加密
        user.setPassword(MD5Util.MD5EncodeUtf8(user.getPassword()));
        //插入数据库
         int resultCount = userMapper.insert(user);
         if (resultCount == 0){
             return ServerResponse.createByErrorMessage("主持失败");
         }
        return ServerResponse.createBySuccessMessage("注册成功");
    }

    /**
     * 用户名和邮箱效验
     * @param str
     * @param type
     * @return
     */
    @Override
    public ServerResponse<String> checkVaild(String str, String type) {
        //判断类型是否为空
        if (org.apache.commons.lang3.StringUtils.isNoneBlank(type)) {
            //开始校验
            if (type.equals(Conts.USERNAME)) {
                int resultCount = userMapper.checkUsername(str);
                if (resultCount > 0) {
                    return ServerResponse.createByErrorMessage("用户名已存在");
                }
              } else if (type.equals(Conts.EMAIL)) {
                int resultCount = userMapper.checkEmail(str);
                if (resultCount > 0) {
                    return ServerResponse.createByErrorMessage("邮箱已注册");
                }
            }
        } else{
            return ServerResponse.createByErrorMessage("参数错误");
        }
        return ServerResponse.createBySuccessMessage("校验成功");


    }

    /**
     * 根据用户名找问题
     * @param username
     * @return
     */
    @Override
    public ServerResponse<String> selectQuestion(String username) {

        ServerResponse<String> vaildResponse = this.checkVaild(username, Conts.USERNAME);
        if (vaildResponse.isSuccess()){
            //用户不存在
            return  ServerResponse.createByErrorMessage("用户不存在");
        }
        String question = userMapper.selectQuestionByUsername(username);
        if (org.apache.commons.lang3.StringUtils.isBlank(question)){
            return ServerResponse.createBySuccess(question);
        }
        return ServerResponse.createByErrorMessage("找回密码的问题为空");
    }

    /**
     * 获取验证问题答案是否正确
     * @param username
     * @param question
     * @param answer
     * @return
     */

    @Override
    public ServerResponse<String> checkAnswer(String username, String question, String answer) {
        int resultCount = userMapper.checkAnswer(username, question, answer);
        if (resultCount > 0){
            //说明问题答案用户名想匹配
            String forgetToken = UUID.randomUUID().toString();
            TokenCache.setKey(TokenCache.TOKEN_PREFIX+username,forgetToken);
            return  ServerResponse.createBySuccess(forgetToken);
        }
        return ServerResponse.createByErrorMessage("问题答案不匹配");
    }

    /**
     * 验证问题答案是否正确进行修改密码
     * @param username
     * @param newPassword
     * @param forgetToken
     * @return
     */
    @Override
    public ServerResponse<String> forgetResetPassword(String username, String newPassword, String forgetToken){

        if (org.apache.commons.lang3.StringUtils.isBlank(forgetToken)){
            return ServerResponse.createByErrorMessage("参数错误,token需要传递");
        }
        if (this.checkVaild(username,Conts.USERNAME).isSuccess()){
            return ServerResponse.createByErrorMessage("用户不存在");
        }
        String token = TokenCache.getKye(TokenCache.TOKEN_PREFIX+username);
        if (org.apache.commons.lang3.StringUtils.isBlank(token)){
            return ServerResponse.createByErrorMessage("token无效或者过期");
        }
        if (org.apache.commons.lang3.StringUtils.equals(forgetToken,token)){
            //经过效验进行修改密码
            int rowCount = userMapper.updatePasswordByusername(username,MD5Util.MD5EncodeUtf8(newPassword));

            if (rowCount > 0){
                return ServerResponse.createBySuccessMessage("修改密码成功");
            }

        } else {
            return ServerResponse.createByErrorMessage("token错误,请重新获取充值密码的token");
        }
        return ServerResponse.createByErrorMessage("修改密码失败");

    }

    /**
     * 在线修改密码
     * @param oldPasswod
     * @param newPassword
     * @param user
     * @return
     */
    @Override
    public  ServerResponse<String> resetPassword(String oldPasswod,String newPassword,User user){

        //防止横向越权,需要效验一下这个用户的旧密码,一定要指定是这个用户的,所以我们会查询一个count(1),如果不知道id,
        // 那么结果就是true 或count > 0
        int resultCount = userMapper.checkPassword(oldPasswod,user.getId());
        if (resultCount  == 0){
            return ServerResponse.createByErrorMessage("旧密码错误");
        }
        user.setPassword(MD5Util.MD5EncodeUtf8(newPassword));
        //执行修改
        int updateCount = userMapper.updateByPrimaryKeySelective(user);
        if (updateCount > 0){
            return ServerResponse.createBySuccessMessage("密码更新成功");
        }
        return ServerResponse.createByErrorMessage("密码更新失败");
    }

    /**
     * 在线修改个人信息
     * @param user
     * @return
     */
    @Override
    public ServerResponse<User> updateInformation(User user){
        //username不能进行更新
        //email 也进行一个效验
        int resultCount = userMapper.checkEmailByUserId(user.getEmail(),user.getId());

        if (resultCount > 0){
            return ServerResponse.createByErrorMessage("email已存在请更换email在尝试更新");
        }
        User updateUser = new User();

        updateUser.setId(user.getId());
        updateUser.setEmail(user.getEmail());
        updateUser.setPhone(user.getPhone());
        updateUser.setQuestion(user.getQuestion());
        updateUser.setAnswer(user.getAnswer());

        int updateCount = userMapper.updateByPrimaryKeySelective(updateUser);

        if (updateCount > 0){
            return ServerResponse.createBySuccess("更新信息成功",updateUser);
        }
        return ServerResponse.createByErrorMessage("更新个人信息失败");
    }

    /**
     *
     * @param userId
     * @return
     */
    @Override
    public  ServerResponse<User> getInformation(Integer userId){
        User user = userMapper.selectByPrimaryKey(userId);
        if ( user == null){
            return ServerResponse.createByErrorMessage("找不到当前用户");
        }
        //将用户密码置空
        user.setPassword(StringUtils.EMPTY);
        return ServerResponse.createBySuccess(user);
    }
    //backend

    /**
     * 效验是否是管理员
     * @param user
     * @return
     */
    public ServerResponse checkAdmininRole(User user){
        if (user != null && user.getRole().intValue() == Conts.Rolw.RoLE_ADMIN){
            return  ServerResponse.createBySuccess();
        }
        else {
            return  ServerResponse.createByErrorMessage();
        }

    }

}
