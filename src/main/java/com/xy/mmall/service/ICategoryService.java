package com.xy.mmall.service;

import com.xy.mmall.common.ServerResponse;
import com.xy.mmall.pojo.Category;

import java.util.List;


/**
 * @Auther: xy
 * @Date: 2018/11/11 22:10
 * @Description:
 */

public interface ICategoryService {

   ServerResponse addCategory(String categoryName,Integer parentId);

   ServerResponse updateCategoryName(Integer categoryId,String categoryName);

   ServerResponse<List<Category>> getChildrenParallCategory(Integer categoryId);

   ServerResponse selectCategoryAndChildrenById(Integer categoryId);

}
