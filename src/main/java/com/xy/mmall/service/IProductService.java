package com.xy.mmall.service;

import com.github.pagehelper.PageInfo;
import com.xy.mmall.common.ServerResponse;
import com.xy.mmall.pojo.Product;
import com.xy.mmall.vo.ProductDetailVo;

/**
 * @Auther: xy
 * @Date: 2018/11/15 15:16
 * @Description:
 */
public interface IProductService {

    ServerResponse saveOrUpdateProduct(Product product);

    ServerResponse<String> setSaleStatus(Integer productId,Integer status );

    ServerResponse<ProductDetailVo> manageProductDetail(Integer productId);

    ServerResponse<PageInfo> getProductList(int pageNum, int pageSize);

    ServerResponse<PageInfo> searchProducts(String productName,Integer productId,Integer pageNum,Integer pageSize);

}
