package com.xy.mmall.service;

import org.springframework.web.multipart.MultipartFile;

/**
 * @Auther: xy
 * @Date: 2018/12/2 22:22
 * @Description:
 */
public interface IFileService {

    String upload (MultipartFile file, String path);
}
