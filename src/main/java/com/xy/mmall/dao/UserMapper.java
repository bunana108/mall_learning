package com.xy.mmall.dao;

import com.xy.mmall.pojo.User;
import org.apache.ibatis.annotations.Param;

public interface UserMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(User record);

    int insertSelective(User record);

    User selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(User record);

    int updateByPrimaryKey(User record);

    int checkUsername(String username);

    User selectUser(@Param("username") String username, @Param("password") String password);

    int checkEmail(String email);

    String selectQuestionByUsername (String username);

    int checkAnswer(@Param("username") String username,@Param("question") String question ,@Param("answer") String answer);

    int updatePasswordByusername(@Param("username") String username,@Param("nwePassword") String newPassword);

    int checkPassword(@Param("password") String oldPassword,@Param("userId") Integer userId);

    int checkEmailByUserId(@Param("email") String email ,@Param("userId") Integer userId);
}