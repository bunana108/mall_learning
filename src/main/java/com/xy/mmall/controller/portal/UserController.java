package com.xy.mmall.controller.portal;

import com.xy.mmall.common.Conts;
import com.xy.mmall.common.ResponseCode;
import com.xy.mmall.common.ServerResponse;
import com.xy.mmall.pojo.User;
import com.xy.mmall.service.IUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpSession;

/**
 * @Auther: xy
 * @Date: 2018/10/23 09:12
 * @Description:
 */
@RestController
@RequestMapping("/user/")
public class UserController {
    @Autowired
    private IUserService iUserService;
    /**
     *用户登录
     * @param username
     * @param password
     * @param session
     * @return
     */
    @RequestMapping(value = "login.do",method = RequestMethod.POST)
    public ServerResponse<User> login(String username, String password, HttpSession session){

        ServerResponse<User> response = iUserService.login(username, password);
        if (response.isSuccess()){
            session.setAttribute(Conts.CURRENT_USER,response.getDate());
        }
        return  response;
    }

    /**
     * 退出
     * @param session
     * @return
     */
    @RequestMapping(value = "logout.do" ,method = RequestMethod.POST)
    public ServerResponse<String> logOut(HttpSession session){
        session.removeAttribute(Conts.CURRENT_USER);
        return ServerResponse.createBySuccess();
    }

    /**
     * 注册
     * @param user
     * @return
     */
    @RequestMapping(value = "register.do",method = RequestMethod.POST)
    public ServerResponse<String> register(User user){
        System.out.println(user);
        return iUserService.register(user);
    }

    /**
     * 用户名和邮箱效验
     * @param str
     * @param type
     * @return
     */
    @RequestMapping(value = "check_valid",method = RequestMethod.POST)
    public ServerResponse<String> checkValid(String str,String type){

        return iUserService.checkVaild(str,type);
    }

    /**
     * 获取登录用户信息
     * @param session
     * @return
     */
    @RequestMapping(value = "get_user_info.do",method = RequestMethod.POST)
    public  ServerResponse<User> getUserInfo(HttpSession session){
        //从session 获取用户
        User user = (User)session.getAttribute(Conts.CURRENT_USER);
        //判断用户为空
        if (user != null){
            return ServerResponse.createBySuccess(user);
        }
        return ServerResponse.createByErrorMessage("当前用户未登录,无法获取信息");
    }

    /**
     *获取验证问题
     * @param usernaem
     * @return
     */
    @RequestMapping(value = "for_get_question.do",method = RequestMethod.POST)
    public ServerResponse<String> forgetGetQuestion(String usernaem){

        return  iUserService.selectQuestion(usernaem);
    }

    /**
     * 验证答案问题是否正确
     * @param username
     * @param question
     * @param answer
     * @return
     */
    @RequestMapping(value = "for_get_check_answer.do",method = RequestMethod.POST)
    public ServerResponse<String> forgetCheckAnswer(String username,String question,String answer){
         return iUserService.checkAnswer(username,question,answer);
    }

    /**
     * 同构问题答案重置密码
     * @param username
     * @param newPassword
     * @param forgetToken
     * @return
     */
    @RequestMapping(value = "for_get_reset_password.do",method = RequestMethod.POST)
    public ServerResponse<String> forgetResetPassword(String username,String newPassword,String forgetToken){

        return iUserService.forgetResetPassword(username,newPassword,forgetToken);

    }

    /**
     * 在线修改密码
     * @param session
     * @param oldPassword
     * @param newPassword
     * @return
     */
    @RequestMapping(value = "reset_password.do",method = RequestMethod.POST)
    public ServerResponse<String> resetPassword(HttpSession session,String oldPassword ,String newPassword){
        //判断用户是否在线
        User user = (User) session.getAttribute(Conts.CURRENT_USER);
        if (user == null){

            return ServerResponse.createByErrorMessage("用户未登陆");
        }
        return  iUserService.resetPassword(oldPassword,newPassword,user);
    }

    /**
     * 在线修改个人信息
     * @param session
     * @param user
     * @return
     */
    @RequestMapping(value = "update_information.do",method = RequestMethod.POST)
    public ServerResponse<User> update_information(HttpSession session,User user){
        //判断用户是否在线
        User currentUser = (User) session.getAttribute(Conts.CURRENT_USER);
        if (currentUser == null){

            return ServerResponse.createByErrorMessage("用户未登陆");
        }
        user.setId(currentUser.getId());

        ServerResponse<User> response = iUserService.updateInformation(user);
        if (response.isSuccess()){
            //将更新后的user放入session
            session.setAttribute(Conts.CURRENT_USER,response.getDate());
        }
        return response;
    }

    public  ServerResponse<User> get_information(HttpSession session){
        //判断用户是否在线
        User currentUser = (User) session.getAttribute(Conts.CURRENT_USER);
        if (currentUser == null){

            return ServerResponse.createByErrorCodeMessage(ResponseCode.NEED_LOGIN.getCode(),"用户未登陆,需要强制登陆 status= 10");
        }
        return iUserService.getInformation(currentUser.getId());


    }

}
