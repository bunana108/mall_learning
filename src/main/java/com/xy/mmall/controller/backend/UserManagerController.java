package com.xy.mmall.controller.backend;


import com.xy.mmall.common.Conts;
import com.xy.mmall.common.ServerResponse;
import com.xy.mmall.pojo.User;
import com.xy.mmall.service.IUserService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpSession;
import java.lang.reflect.Constructor;

/**
 * @Auther: xy
 * @Date: 2018/10/23 22:25
 * @Description:
 */
@RestController
@RequestMapping("manager/user/")
public class UserManagerController {

    @Autowired
    private IUserService iUserService;

    /**
     * 管理员登陆
     * @param username
     * @param passwrod
     * @param session
     * @return
     */
    @RequestMapping(value = "login.do" ,method = RequestMethod.POST )
    public ServerResponse<User> login(String username, String passwrod, HttpSession session){

        ServerResponse<User> response = iUserService.login(username, passwrod);

        if (response.isSuccess()){
            User user = response.getDate();
            if (user.getRole().equals(Conts.Rolw.RoLE_ADMIN)){
                //说明是管理员
                session.setAttribute(Conts.CURRENT_USER,user);
                return response;
            }
            else {
                return ServerResponse.createByErrorMessage("不是管理员权限不足无法登陆");
            }
        }
        return response;
    }
}
