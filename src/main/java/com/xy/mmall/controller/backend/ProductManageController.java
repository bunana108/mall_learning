package com.xy.mmall.controller.backend;


import com.google.common.collect.Maps;
import com.xy.mmall.common.Conts;
import com.xy.mmall.common.ResponseCode;
import com.xy.mmall.common.ServerResponse;
import com.xy.mmall.pojo.Product;
import com.xy.mmall.pojo.User;
import com.xy.mmall.service.IFileService;
import com.xy.mmall.service.IProductService;
import com.xy.mmall.service.IUserService;
import com.xy.mmall.util.PropertiesUtil;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.util.Map;

/**
 * @Auther: xy
 * @Date: 2018/11/15 15:15
 * @Description:
 */
@RestController
@RequestMapping("/manage/product")
public class ProductManageController {

    @Autowired
    private IProductService iProductService;

    @Autowired
    private IUserService iUserService;

    @Autowired
    private IFileService iFileService;

    /**
     * 更新或新增产品
     *
     * @param session
     * @param product
     * @return
     */
    @RequestMapping("/save.do")
    public ServerResponse productSave(HttpSession session, Product product) {
        //判断用户是否登录
        User user = (User) session.getAttribute(Conts.CURRENT_USER);
        if (user == null) {
            return ServerResponse.createByErrorCodeMessage(ResponseCode.NEED_LOGIN.getCode(), "请登录");
        }
        //判断是不是管理员权限
        if (iUserService.checkAdmininRole(user).isSuccess()) {
            return iProductService.saveOrUpdateProduct(product);
        }
        return ServerResponse.createByErrorMessage("您无权操作");
    }

    /**
     * 更改产品销售状态
     *
     * @param session
     * @param productId
     * @param status
     * @return
     */
    @RequestMapping("set_sale_status.do")
    public ServerResponse setSaleStatus(HttpSession session, Integer productId, Integer status) {
        //判断用户是否登录
        User user = (User) session.getAttribute(Conts.CURRENT_USER);
        if (user == null) {
            return ServerResponse.createByErrorCodeMessage(ResponseCode.NEED_LOGIN.getCode(), "请登录");
        }
        //判断是不是管理员权限
        if (iUserService.checkAdmininRole(user).isSuccess()) {
            return iProductService.setSaleStatus(productId, status);
        }
        return ServerResponse.createByErrorMessage("您无权操作");
    }

    /**
     * 获取产品详情
     *
     * @param session
     * @param productId
     * @return
     */
    @RequestMapping("detail.do")
    public ServerResponse getDetail(HttpSession session, Integer productId) {
        //判断用户是否登录
        User user = (User) session.getAttribute(Conts.CURRENT_USER);
        if (user == null) {
            return ServerResponse.createByErrorCodeMessage(ResponseCode.NEED_LOGIN.getCode(), "请登录");
        }
        //判断是不是管理员权限
        if (iUserService.checkAdmininRole(user).isSuccess()) {
            return iProductService.manageProductDetail(productId);
        }
        return ServerResponse.createByErrorMessage("您无权操作");
    }

    @RequestMapping("list.do")
    public ServerResponse getList(HttpSession session, @RequestParam(value = "pageNum", defaultValue = "1") Integer pageNum, @RequestParam(value = "pageSize", defaultValue = "10") Integer pageSize) {

        //判断用户是否登录
        User user = (User) session.getAttribute(Conts.CURRENT_USER);
        if (user == null) {
            return ServerResponse.createByErrorCodeMessage(ResponseCode.NEED_LOGIN.getCode(), "请登录");
        }
        //判断是不是管理员权限
        if (iUserService.checkAdmininRole(user).isSuccess()) {
            return iProductService.getProductList(pageNum, pageSize);
        }
        return ServerResponse.createByErrorMessage("您无权操作");
    }

    @RequestMapping("search.do")
    public ServerResponse productSearch(HttpSession session, String productName, Integer productId, @RequestParam(value = "pageNum", defaultValue = "1") Integer pageNum, @RequestParam(value = "pageSize", defaultValue = "10") Integer pageSize) {

        //判断用户是否登录
        User user = (User) session.getAttribute(Conts.CURRENT_USER);
        if (user == null) {
            return ServerResponse.createByErrorCodeMessage(ResponseCode.NEED_LOGIN.getCode(), "请登录");
        }
        //判断是不是管理员权限
        if (iUserService.checkAdmininRole(user).isSuccess()) {
            //填充业务逻辑
            return iProductService.searchProducts(productName, productId, pageNum, pageSize);

        }
        return ServerResponse.createByErrorMessage("您无权操作");
    }

    @RequestMapping("upload.do")
    public ServerResponse upload(HttpSession session, @RequestParam(value = "upload_file", required = false) MultipartFile file, HttpServletRequest request) {

        //判断用户是否登录
        User user = (User) session.getAttribute(Conts.CURRENT_USER);
        if (user == null) {
            return ServerResponse.createByErrorCodeMessage(ResponseCode.NEED_LOGIN.getCode(), "请登录");
        }
        //判断是不是管理员权限
        if (iUserService.checkAdmininRole(user).isSuccess()) {
            //填充业务逻辑
            String path = request.getSession().getServletContext().getRealPath("upload");
            String targetFileName = iFileService.upload(file, path);
            String url = PropertiesUtil.getProperty("ftp.server.http.prefix") + targetFileName;
            Map fileMap = Maps.newHashMap();
            fileMap.put("uri", targetFileName);
            fileMap.put("url", url);
            return ServerResponse.createBySuccess(fileMap);

        }
        return ServerResponse.createByErrorMessage("您无权操作");
    }

    @RequestMapping("richtext_img_upload.do")
    public Map richtextImgUploadupload(HttpSession session, @RequestParam(value = "upload_file", required = false) MultipartFile file, HttpServletRequest request, HttpServletResponse response) {

        Map map = Maps.newHashMap();
        //判断用户是否登录
        User user = (User) session.getAttribute(Conts.CURRENT_USER);
        if (user == null) {
            map.put("success",false);
            map.put("msg","请登录管理员");

            return map;
        }
        //判断是不是管理员权限
        //富文本中对于返回值有自己的要求,我们使用的是simditor所有按照simdtor的要求返回
        /**
         * {
         * "success": true / false
         * "msg": "error message" ,#optional
         * "file_path" "[]real file path"
         * }
         */
        if (iUserService.checkAdmininRole(user).isSuccess()) {

            //填充业务逻辑
            String path = request.getSession().getServletContext().getRealPath("upload");
            String targetFileName = iFileService.upload(file, path);

            if (StringUtils.isBlank(targetFileName)){
                map.put("success",false);
                map.put("msg","上传失败");
                return  map;
            }
            String url = PropertiesUtil.getProperty("ftp.server.http.prefix") + targetFileName;
            Map fileMap = Maps.newHashMap();
            fileMap.put("uri", targetFileName);
            fileMap.put("url", url);
            map.put("success",true);
            map.put("msg","上传成功");
            map.put("file_path",targetFileName);
            response.addHeader("Access-Control-Allow-Headers","X-File-Name");
            return map;

        }
        map.put("success",false);
        map.put("msg","无权限操作");
        return  map;

    }








}
