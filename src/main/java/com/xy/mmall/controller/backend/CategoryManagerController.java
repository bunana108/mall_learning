package com.xy.mmall.controller.backend;

import com.xy.mmall.common.Conts;
import com.xy.mmall.common.ResponseCode;
import com.xy.mmall.common.ServerResponse;
import com.xy.mmall.pojo.User;
import com.xy.mmall.service.ICategoryService;
import com.xy.mmall.service.IUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpSession;

/**
 * @Auther: xy
 * @Date: 2018/11/11 21:51
 * @Description:
 */
@RestController
@RequestMapping("/manage/category")
public class CategoryManagerController {

    @Autowired
    private IUserService iUserService;
    @Autowired
    private ICategoryService iCategoryService;

    /**
     * 添加分类
     * @param session
     * @param categoryName
     * @param parentId
     * @return
     */
    @RequestMapping("add_category.do")
    public ServerResponse addCategory(HttpSession session, String categoryName, @RequestParam(value = "parentId", defaultValue = "0") int parentId) {

        User user = (User) session.getAttribute(Conts.CURRENT_USER);
        if (user == null) {
            return ServerResponse.createByErrorCodeMessage(ResponseCode.NEED_LOGIN.getCode(),"用户未登录,请登录");
        }
        //判断是否是管理员
        if (iUserService.checkAdmininRole(user).isSuccess()){
            //是管理员

            //增加我们的处理分类逻辑
            return   iCategoryService.addCategory(categoryName,parentId);
        }
        else{
            return  ServerResponse.createByErrorMessage("无权限操作需要管理员权限");
        }
    }

    /**
     * 修改商品信息
     * @param session
     * @param categoryId
     * @param categoryName
     * @return
     */
    @RequestMapping("set_category.do")
    public ServerResponse setCategoryName(HttpSession session,Integer categoryId,String categoryName){
        User user = (User) session.getAttribute(Conts.CURRENT_USER);
        if (user == null) {
            return ServerResponse.createByErrorCodeMessage(ResponseCode.NEED_LOGIN.getCode(),"用户未登录,请登录");
        }
        //判断是否是管理员
        if (iUserService.checkAdmininRole(user).isSuccess()){
            //是管理员

            //增加我们的处理分类逻辑
            return   iCategoryService.updateCategoryName(categoryId,categoryName);
        }
        else{
            return  ServerResponse.createByErrorMessage("无权限操作需要管理员权限");
        }
    }

    /**
     * 查询子节点
     * @param session
     * @param categoryId
     * @return
     */
    @RequestMapping("get_category.do")
    public ServerResponse getChildrenParallelCategory(HttpSession session,@RequestParam(value = "categoryId", defaultValue = "0") int categoryId){

        User user = (User) session.getAttribute(Conts.CURRENT_USER);
        if (user == null) {
            return ServerResponse.createByErrorCodeMessage(ResponseCode.NEED_LOGIN.getCode(),"用户未登录,请登录");
        }
        //判断是否是管理员
        if (iUserService.checkAdmininRole(user).isSuccess()){
            System.out.println(categoryId);
            //是管理员

            //查询子节点的category信息,并且不递归,保持平级
            return   iCategoryService.getChildrenParallCategory(categoryId);
        }
        else{
            return  ServerResponse.createByErrorMessage("无权限操作需要管理员权限");
        }
    }
    @RequestMapping("get_deep_category.do")
    public ServerResponse getCategoryAndDeepChildrenParallelCategory(HttpSession session,@RequestParam(value = "categoryId", defaultValue = "0") int categoryId){

        User user = (User) session.getAttribute(Conts.CURRENT_USER);
        if (user == null) {
            return ServerResponse.createByErrorCodeMessage(ResponseCode.NEED_LOGIN.getCode(),"用户未登录,请登录");
        }
        //判断是否是管理员
        if (iUserService.checkAdmininRole(user).isSuccess()){

            //是管理员

            //查询当前节点的id和递归子节点的Id,
            return   iCategoryService.selectCategoryAndChildrenById(categoryId);
        }
        else{
            return  ServerResponse.createByErrorMessage("无权限操作需要管理员权限");
        }
    }
    @RequestMapping("/aaa.do")
    public  String add(){
        return  "add";
    }
}